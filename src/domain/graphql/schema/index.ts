import { gql } from 'apollo-server-express';

const typeDefs = gql`

type LoginResult {
  success: Boolean!
  message: String
  token: String
}

type Query {
  dummy: String
}

type Mutation {
  authenticate(login: String!, password: String!): LoginResult!
}


`;

export default typeDefs