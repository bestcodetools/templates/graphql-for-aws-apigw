import { ApolloServer } from 'apollo-server-express';
import express from 'express';
import http from 'http';
import cors from 'cors';
import compression from 'compression';
import typeDefs from 'src/domain/graphql/schema';
import resolvers from 'src/infrastructure/graphql/resolvers';
import { makeExecutableSchema } from 'graphql-tools';

const app = express();

const emit = app.emit;
app.emit = (...args) => {
  console.log('emit:', ...args);
  return emit.bind(app)(...args);
}

app.use(cors());
app.use(compression());

let graphqlServerStatus = { statusCode: 423, body: 'not-loaded' }
app.all('/graphql/liveness', (_req, res) => res.status(graphqlServerStatus.statusCode).send(graphqlServerStatus.body));

const graphqlServer = new ApolloServer({
  schema: makeExecutableSchema({ typeDefs, resolvers })
});

graphqlServer.start().then(() => {
  graphqlServerStatus.statusCode = 200;
  graphqlServerStatus.body = 'OK';
  graphqlServer.applyMiddleware({ app });
});

export default app;

