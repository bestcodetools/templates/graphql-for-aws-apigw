import { AddressInfo } from 'net';
import app from './app';

const server = app.listen(4000, () => {
  console.log('GraphQL server is now live on port ' + (server.address() as AddressInfo).port)
});
